FROM scratch

LABEL maintaner="Miroslav Rubish <miroslav.rubish@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
